<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */



$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

if( is_page('home') ) {
  $about = get_page_by_path('about-us');
  $about = strip_tags($about->post_content);
  $context['about'] = make_excerpt($about, 350);
}
 if( is_page('results') ) {
   $context['disclaimer'] = get_field('calculator_disclaimer', 10);
   if( isset($_GET['vis']) ) {
    $context['vis'] = $_GET['vis'];
    $context['acid'] = $_GET['acid'];
    $context['water'] = $_GET['water'];
    $context['resist'] = $_GET['resist'];
   }
 }
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page.twig' ), $context );

function make_excerpt($text, $length){
  if(strlen($text) > $length) {
    $text = substr($text, 0, strpos($text, ' ', $length));
  }
  return $text;
}