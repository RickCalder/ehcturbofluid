

'use strict'
navbar()
if( window.location.pathname === '/faq/') {
  var anyActive = false;
  $('.question').first()
    .addClass('active')
    .next().show()
  $('.question').first().find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
  $(".question").on("click", function() {
    if(! $(this).hasClass('active') ) {
      $('.question').removeClass('active')
      $(this).addClass('active')
      $(".question").find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      $(".answer").slideUp().removeClass('active')
      $(this).next().slideDown()
    } else {
      $(this).removeClass('active')
      $(this).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      $(this).next().slideUp()
    }

  })
}

function equalheight(container){
  var currentTallest = 0
  var currentRowStart = 0
  var rowDivs = new Array()
  var $el
   $(container).each(function() {
     $el = $(this);
     $($el).height('auto')
      var topPostion = $el.position().top;
  
     if (currentRowStart != topPostion) {
       for (var currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
         rowDivs[currentDiv].height(currentTallest);
       }
       rowDivs.length = 0; // empty the array
       currentRowStart = topPostion;
       currentTallest = $el.height();
       rowDivs.push($el);
     } else {
       rowDivs.push($el);
       currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
   }); 
  }

  if( window.location.hash === '' ) {
    $('.ehc-calculator-link').removeClass('active')
    $('.products-link').removeClass('active')
  } else if( window.location.hash === '#products' ){
    $('.ehc-calculator-link').removeClass('active')
    $('.home-link').removeClass('active')
  } else if( window.location.hash === '#calculator' ){
    $('.products-link').removeClass('active')
    $('.home-link').removeClass('active')
  }

  $('.nav-link').on('click', function() {
    if( $(this).hasClass('products-link') ) {
      $('.nav-link').removeClass('active')
      $('.products-link').addClass('active')
    }
  })
  $('.nav-link').on('click', function() {
    if( $(this).hasClass('ehc-calculator-link') ) {
      $('.nav-link').removeClass('active')
      $('.ehc-calculator-link').addClass('active')
    }
  })
  if( window.location.pathname === '/' || window.location.pathname === '/#products' || window.location.pathname === '/#calculator' ) {
    equalheight('.equal-height')	
    
    $('.info').on('click', function(e) {
			e.preventDefault()
			var div = $('.' + $(this).data('div'))

			div.slideToggle()
		})

		$('#testSubmit').on('click', function(e) {
			e.preventDefault();
			$(".error").slideUp()
			var errors = 0
			if(! $.isNumeric($('#viscosity').val())) {
				$(".vis-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#acid').val())) {
				$(".acid-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#resistivity').val())) {
				$(".resist-error").slideDown()
				errors +=1
			}
			if(! $.isNumeric($('#water').val())) {
				$(".water-error").slideDown()
				errors +=1
			}
			if(errors > 0) {
				return false
			}
			var vis = $('#viscosity').val()
			var acid = $('#acid').val()
			var resist = $('#resistivity').val()
			var water = $('#water').val()
			var result = '?vis=' + vis + '&acid=' + acid + '&water=' + water + '&resist=' + resist
			document.location.href = '/results' + result;
			console.log(result)
		})
  }

  if( window.location.pathname === '/results/') {
    var results = getUrlVars();
		console.log(results);
		var vis = parseFloat(results['vis']).toFixed(1)
		var acid = parseFloat(results['acid']).toFixed(2)
		var water = parseFloat(results['water']).toFixed(0)
		var resistivity = parseFloat(results['resist']).toFixed(1)
		console.log(vis)
    $(".resist-value").html(resistivity)
    $(".water-value").html(water)
    $(".acid-value").html(acid)
    $(".vis-value").html(vis)

		if(vis == 'NaN') {
			document.location.href = '/#calculator'
		}

		switch(true) {
			case 'secondary' :
				$('.viscosity.secondary').css('display','block');
				break;
			case (vis >= 43.7 && vis <= 48.3) :
				$('.viscosity.least').css('display','block');
				break;
			case ((vis >= 42.6 && vis <= 43.6) || (vis >= 48.4 && vis <= 49.5)) :
				$('.viscosity.less').css('display','block');
				break;
			case ((vis >= 41.4 && vis <= 42.5) || (vis >= 49.6 && vis <= 50.6)) :
				$('.viscosity.caution').css('display','block');
				break;
			case ((vis >= 39.1 && vis <= 41.3) || (vis >= 50.7 && vis <= 52.9)) :
				$('.viscosity.more').css('display','block');
				break;
			case (vis <= 39 || vis >= 53) :
				$('.viscosity.most').css('display','block');
				break;
      default:
        console.log(vis)
		}

    switch(true) {
      case 'secondary' :
        $('.water.secondary').css('display','block');
        break;
      case (water < 200) :
        $('.water.least').css('display','block');
        break;
      case (water >=200 && water <=599) :
        $('.water.less').css('display','block');
        break;
      case (water >=600 && water <=999) :
        $('.water.caution').css('display','block');
        break;
      case (water >= 1000 && water <= 1500) :
        $('.water.more').css('display','block');
        break;
      case (water > 1500) :
        $('.water.most').css('display','block');
        break;
    }

    switch(true) {
      case 'secondary' :
        $('.acid.secondary').css('display','block');
        break;
      case (acid <= 0.1) :
        $('.acid.least').css('display','block');
        break;
      case (acid > 0.1 && acid <=0.15) :
        $('.acid.less').css('display','block');
        break;
      case (acid > 0.15 && acid <= 0.2) :
        $('.acid.caution').css('display','block');
        break;
      case (acid > 0.2 && acid <= 0.25) :
        $('.acid.more').css('display','block');
        break;
      case (acid > 0.25) :
        $('.acid.most').css('display','block');
        break;
    }



    switch(true) {
      case 'secondary' :
        $('.resistivity.secondary').css('display','block');
        break;
      case (resistivity > 20) :
        $('.resistivity.least').css('display','block');
        break;
      case (resistivity >=10 && resistivity <= 20) :
        $('.resistivity.less').css('display','block');
        break;
      case (resistivity >=5 && resistivity <= 9) :
        $('.resistivity.caution').css('display','block');
        break;
      case (resistivity >=1 && resistivity <= 4) :
        $('.resistivity.more').css('display','block');
        break;
      case (resistivity < 1) :
        $('.resistivity.most').css('display','block');
        break;
    }

	function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++){
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  }
  }

  if(window.location.pathname == '/contact-us/') {
    var prod = getUrlVars()
    console.log(prod)
    if( prod['product']) {
      $('#subject').val( 'Please send me the TDS and/or the SDS for ' + decodeURIComponent(prod['product']) )
    } else if( prod['compare'] ) {
      $('#subject').val( 'Please send me the product comparison for Turbofluid 46B' )
    }
  }

  if( window.location.pathname == '/' ) {
    var queryString = getUrlVars()
    console.log(queryString)
    if( queryString['vis'] ) {
      $('#viscosity').val(queryString['vis'])
      $('#acid').val(queryString['acid'])
      $('#resistivity').val(queryString['resist'])
      $('#water').val(queryString['water'])
    }
  }
  
  $(".scroll-link").on("click", function(e) {
    e.preventDefault();
    var $target = $(this).attr('href').replace('/', '');
    if($($target).length) {
      scrollToLink($target)
    } else {
      window.location.href = $(this).attr('href')
    }
  });

  function scrollToLink(target) {
    $(".navbar-collapse").collapse('hide');
    var offset = 137
    $("html, body").animate({
      scrollTop: $(target).offset().top - offset
    }, 500)
  }
  $(window).on('load', function(e){
    e.preventDefault()
    if(window.location.hash) {
      scrollToLink(window.location.hash)
    }
    
  })
  $(window).scroll(_.debounce(function(){
    navbar()
  }, 100))
  
  $(window).resize(_.debounce(navbar))

  function navbar() {
    if($(document).scrollTop() > 100 || $(window).width() < 992) {
      $('.navbar').css('background-color', 'rgba(234, 29, 47, 1)')
    } else {
      $('.navbar').css('background-color', 'rgba(234, 29, 47, 0.8)')
    }
  }

  $('.calculator-background-trigger').on('click', function(e) {
    e.preventDefault()
    if( $(this).hasClass('open') ){
      $(this).removeClass('open')
      $(this).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
      $('.calculator-background').slideUp()
    } else {
      $(this).addClass('open')
      $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
      $('.calculator-background').slideDown()
      scrollToLink('#background')
    }
  })