jQuery(document).ready(function ($) {
  var elementExists = document.getElementById("slider");
  if( elementExists === null ) {
    return
  }
  $("#slider").fadeIn()
  var isRunning = true
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width()
	var slideHeight = $('#slider ul li').height();
  var sliderUlWidth = slideCount * slideWidth;
  var slideInterval = 6000

  // Set timer

  if( $.isNumeric($('#slide-interval').text()) ) {
    slideInterval = $('#slide-interval').text() * 1000
  } else {
    slideInterval = 6000
  }
  var slideTimer = setInterval(sliderTime, slideInterval);

  function sliderTime() {
    moveRight();
  }

  //Pause on hover
  $('#slider ul').hover(function() {
    isRunning = false
  }, function() {
    isRunning = true
  });
	$('#slider').css({ width: slideWidth, height: slideHeight });
  $('#slider ul li').css('width', $(window).width())
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 600, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
      if( isRunning == false ) return
      $('#slider ul').animate({
          left: - slideWidth
      }, 600, function () {
          $('#slider ul li:first-child').appendTo('#slider ul');
          $('#slider ul').css('left', '');
          $('#slider ul li').css('width', $(window).width())
      });
    };

    $('a.control_prev').click(function () {
      clearInterval(slideTimer)
      slideTimer = setInterval(sliderTime, slideInterval);
      moveLeft();
    });

    $('a.control_next').click(function () {
      clearInterval(slideTimer)
      slideTimer = setInterval(sliderTime, slideInterval);
      moveRight();
    });
    
    $(window).resize( _.throttle(function() {
      var winWidth = $(window).width()
      $('#slider ul li').css('width', winWidth)
      $('#slider').css('width', winWidth)
      slideWidth = winWidth
      sliderUlWidth = slideCount * slideWidth;
      $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
      if( winWidth < 576 ) {
        slideHeight = 300
        $('#slider').css('height', '300px')
      } else if( winWidth >= 576 && winWidth < 768 ){
        slideHeight = 350
        $('#slider').css('height', '350px')
      } else if( winWidth >= 768 && winWidth < 992 ){
        slideHeight = 400
        $('#slider').css('height', '400px') 
      } else if( winWidth >= 992 && winWidth < 1200 ){
        slideHeight = 500
        $('#slider').css('height', '500px')
      } else {
        slideHeight = 600
        $('#slider').css('height', '600px')
      }
      
    }, 100))

});    
