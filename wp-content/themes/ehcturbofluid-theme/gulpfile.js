'use strict';

// Include Gulp and plugins
var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

// Include additional tools
var runSequence = require('run-sequence');

gulp.task('styles:compile', function() {
  return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'assets/src/scss/**/*.scss'])
    .pipe($.sass())
    .pipe(gulp.dest('assets/src/css'))
    .pipe($.size({title: 'styles:compile'}));
});

gulp.task('styles:compress', function() {
  return gulp.src(['assets/src/css/**/*.css'])
    .pipe($.concatCss('main.css'))
    .pipe($.autoprefixer('last 2 versions'))
    .pipe($.rename('main.min.css'))
    .pipe($.csso())
    .pipe(gulp.dest('assets/dist/css'))
    // .pipe($.size({title: 'styles:compress'}));
});

gulp.task('styles', function(next) {
  return runSequence('styles:compile', 'styles:compress', next);
});

gulp.task('scripts:compress', function() {
  return gulp.src([
      // 'node_modules/bootstrap/js/**/*.js',
      'assets/src/js/*.js'
    ])
    .pipe($.sourcemaps.init({loadMaps: true}))
    .pipe($.concat('main.min.js'))
    .pipe($.uglify())
    .pipe($.chmod(755))
    .pipe(gulp.dest('assets/dist/js'))
    // .pipe($.size({title: 'scripts:compress'}));
});

// gulp.task('scripts:move_standalone', function() {
//   return gulp.src([
//       'assets/src/js_standalone/*.js'
//     ])
//     .pipe($.sourcemaps.init({loadMaps: true}))
//     .pipe($.rename(function (path) {
//       path.suffix += ".min";
//     }))
//     .pipe($.uglify())
//     .pipe($.chmod(755))
//     .pipe(gulp.dest('assets/dist/js'))
// });

gulp.task('scripts', function(next) {
  return runSequence('scripts:compress', next);
});

/**
 * Run styles, scripts, and fonts in parallel
 * Run callback function
 */
gulp.task('build', function(next) {
  return runSequence(['styles', 'scripts'], next);
});

gulp.task('default', function() {
  gulp.start('build', function() {
    gulp.watch('assets/vendor/js/**/*.js', ['scripts']);
    gulp.watch('assets/src/js/**/*.js', ['scripts']);
    gulp.watch('assets/vendor/css/**/*.css', ['styles']);
    gulp.watch('assets/vendor/scss/**/*.scss', ['styles']);
    gulp.watch('assets/src/scss/**/*.scss', ['styles']);

  });
});
